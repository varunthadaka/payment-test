package com.payment.test.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/payment-test")
@Slf4j
public class CustomerController {


	@GetMapping("/{id}")
	@Secured("ROLE_USER") 
	public String GetCustomer(@PathVariable Long id) {
		log.info("Customer Id : {}", id); 
		return "Customer" + id; 
	}


	@PostMapping(value = "/processOrderNotification")
	@Secured("ROLE_USER")
	public String processOrderNotification(@RequestBody String orderNotificationString) {

		log.info("Received Order notification : {}", orderNotificationString);
		return "Received order notification !!!";
	}
}
